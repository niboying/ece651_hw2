import java.util.*;

public class Person{
	protected
	String firstnm;
	String lastnm;
	String nationality;
	String presentPosition;
	String background;
	String hbbies;
	int id;
	static int idnumber;
	
	public 
	Person(String fnm,String lnm,String nat,String pst,String bg,String hb) {
		firstnm = fnm;
		lastnm = lnm;
		nationality = nat;
		presentPosition = pst;
		background = bg;
		hbbies = hb;
		id=idnumber;
		idnumber++;
	}
	
	void whoIs() {
		if(nationality!=null) {
		System.out.print(firstnm+" "+lastnm+" is from " + nationality+" and is a/an "+presentPosition+".");
		System.out.print(background);
		System.out.println("When in spare time, "+firstnm+" enjoys "+hbbies+".");
		}
		else {
			System.out.println("This person is a mystery.");
		}
	}
	
	static class BlueDevil	extends Person{
		private
		String major;
		String undergraduate;		
		public BlueDevil(String fnm,String lnm,String nat,String mj,String udg,String hb) {
			super(fnm,lnm, nat, null, null, hb);
			major = mj;
			undergraduate = udg;
		}
		
		void whoIs() {
			if(nationality!=null) {
			System.out.print(firstnm+" "+lastnm+" is from " + nationality+" and is a/an "+major+" student. ");
			System.out.print("He/she doesn't have previous work experience. ");
			System.out.print("He/she obtains undergraduate degree in "+undergraduate+". ");
			System.out.println("When in spare time, "+firstnm+" enjoys "+hbbies+".");
			}
			else {
				System.out.println("This person is a mystery.");
			}
		}
	}
	
	public static void main(String[] args) {
		Person ric = new Person("Ric","Telford","the United States","Adjunct Assoc. Professor, Director of DUhatch Business Incubator, Director of Duke Center for Mobile Development",
				"He retired from IBM as Vice President, Cloud Strategy", 
				"golf, sand volleyball, swimming and biking");
		Person adel = new Person("Adel","Fahmy","Eygpt","Adjunct Assistant Professor","He worked for IBM for 30 years","tennis, biking, gardening, and cooking");
		BlueDevil yuanyuan = new BlueDevil("Yuanyuan","Yu","China","MEng ECE","ECUST,China","baseball and fencing");
		BlueDevil you = new BlueDevil("You","Lyu","China","M.S ECE","U.K","traveling, music and history");
		BlueDevil sihao = new BlueDevil("Sihao","Yao",null,null,"","");
		BlueDevil lei = new BlueDevil("Lei","Chen","China","MS ECE","KAIST","climbing and animals");
		BlueDevil shalin = new BlueDevil("Shalin","Shah","India","PhD ECE","DA-IICT","bodybuilding and dancing");
		BlueDevil zhongyu = new BlueDevil("Zhongyu","Li","Dalian, China","Master ECE","SouthEast University","basketball&NBA");
		BlueDevil nibo = new BlueDevil("Nibo","Ying","China","M.S ECE","SISU","basketball and cooking");
		List<String> list1 = new ArrayList<String>();
		list1.add("ric telford");
		list1.add("ric");
		list1.add("telford");
		List<String> list2 = new ArrayList<String>();
		list1.add("adel fahmy");
		list1.add("adel");
		list1.add("fahmy");
		List<String> list3 = new ArrayList<String>();
		list3.add("yuanyuan yu");
		list3.add("yuanyuan");
		list3.add("yu");
		List<String> list4 = new ArrayList<String>();
		list4.add("you lyu");
		list4.add("you");
		list4.add("lyu");
		List<String> list5 = new ArrayList<String>();
		list5.add("sihao yao");
		list5.add("sihao");
		list5.add("yao");
		List<String> list6 = new ArrayList<String>();
		list6.add("lei chen");
		list6.add("lei");
		list6.add("chen");
		List<String> list7 = new ArrayList<String>();
		list7.add("shalin shah");
		list7.add("shalin");
		list7.add("shah");
		List<String> list8 = new ArrayList<String>();
		list8.add("zhongyu li");
		list8.add("zhongyu");
		list8.add("li");
		List<String> list9 = new ArrayList<String>();
		list9.add("nibo ying");
		list9.add("nibo");
		list9.add("ying");
		
		// a map of nameList and Person objects
		Map<List<String>, Person> myMap = new HashMap<List<String>, Person>();
		myMap.put(list1, ric);		
		myMap.put(list2, adel);
		myMap.put(list3, yuanyuan);
		myMap.put(list4, you);
		myMap.put(list5, sihao);
		myMap.put(list6, lei);
		myMap.put(list7, shalin);
		myMap.put(list8, zhongyu);
		myMap.put(list9, nibo);
		
		Scanner reader = new Scanner(System.in);
		String input = "";
		while(true) {
			System.out.println("");
			System.out.println("Please enter a name: ");
			input = (reader.next()).toLowerCase();  // handle case differnce
			if(input.equals("quit")) { // to quit the program
				reader.close();
				return;
				}
			Person name = null;
			for (Map.Entry<List<String>,Person> entry : myMap.entrySet()) { 
	            if(entry.getKey().contains(input)) {  //user input in the nameList
	            	name = entry.getValue(); //return the Person object
	            	}
			}
			if(name==null) {
				System.out.println("This person doesn't exist.");
				continue;
			}
			name.whoIs();	
		}

	}

}

